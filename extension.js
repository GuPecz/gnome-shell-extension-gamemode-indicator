import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';
import {SystemIndicator} from 'resource:///org/gnome/shell/ui/quickSettings.js';

const GAMEMODE_DBUS_NAME = 'com.feralinteractive.GameMode';
const GAMEMODE_DBUS_PATH = '/com/feralinteractive/GameMode';

const GameModeInterface = '<node> \
  <interface name="com.feralinteractive.GameMode"> \
    <property name="ClientCount" type="i" access="read"></property> \
  </interface> \
</node>';

const GameModeIndicator = GObject.registerClass(
  class GameModeIndicator extends SystemIndicator {
      _init() {
          super._init();

          // Create an icon for the indicator
          this._indicator = this._addIndicator();
          this._indicator.icon_name = 'applications-games-symbolic';

          // Create information from the DBus interface
          const gameModeInfo = Gio.DBusInterfaceInfo.new_for_xml(GameModeInterface);

          // Create a connection to the gamemode interface
          this._proxy = new Gio.DBusProxy({
              g_connection: Gio.DBus.session,
              g_name: GAMEMODE_DBUS_NAME,
              g_object_path: GAMEMODE_DBUS_PATH,
              g_interface_name: gameModeInfo.name,
              g_interface_info: gameModeInfo,
          });

          // Watch for changed properties
          this._connection = this._proxy.connect('g-properties-changed', (_proxy, properties) => {
              const clientCountChanged = !!properties.lookup_value('ClientCount', null);
              if (clientCountChanged)
                  this._sync();
          });

          // Init DBus connection
          this._proxy.init_async(GLib.PRIORITY_DEFAULT, null)
            .catch(e => console.error(e.message));

          this._sync();
      }

      _sync() {
          this._indicator.visible = this._proxy.ClientCount > 0;
      }

      destroy() {
          this._indicator.destroy();
          this._indicator = null;
          if (this._connection) {
              this._proxy.disconnect(this._connection);
              this._connection = null;
          }
          this._proxy = null;

          super.destroy();
      }
  });

export default class GameModeIndicatorExtension extends Extension {
    enable() {
        if (this._indicator)
            return;

        this._indicator = new GameModeIndicator();
        Main.panel.statusArea.quickSettings.addExternalIndicator(this._indicator);
    }

    disable() {
        this._indicator.destroy();
        this._indicator = null;
    }
}
